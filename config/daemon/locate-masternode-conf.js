// @flow

import path from 'path';
import os from 'os';

import { app } from '../electron'; // eslint-disable-line

export const locateMasternodeConf = () => {
  if (os.platform() === 'darwin') {
    return path.join(app.getPath('appData'), 'Commercium', 'masternode.conf');
  }

  if (os.platform() === 'linux') {
    return path.join(app.getPath('home'), '.commercium', 'masternode.conf');
  }

  return path.join(app.getPath('appData'), 'Commercium', 'masternode.conf');
};
