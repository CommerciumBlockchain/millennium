// @flow

import cp from 'child_process';
import path from 'path';
import os from 'os';
import fs from 'fs';
import processExists from 'process-exists';
/* eslint-disable import/no-extraneous-dependencies */
import isDev from 'electron-is-dev';
import type { ChildProcess } from 'child_process';
import eres from 'eres';
import uuid from 'uuid/v4';
import findProcess from 'find-process';

/* eslint-disable-next-line import/named */
import { mainWindow } from '../electron';
import waitForDaemonClose from './wait-for-daemon-close';
import getBinariesPath from './get-binaries-path';
import getOsFolder from './get-os-folder';
import getDaemonName from './get-daemon-name';
import fetchParams from './run-fetch-params';
import { locateCommerciumConf } from './locate-commercium-conf';
import { locateMasternodeConf } from './locate-masternode-conf';
import { log } from './logger';
import store from '../electron-store';
import { parseCommerciumConf, parseCmdArgs, generateArgsFromConf } from './parse-commercium-conf';
import { isTestnet } from '../is-testnet';
import {
  EMBEDDED_DAEMON,
  COMMERCIUM_NETWORK,
  TESTNET,
  MAINNET,
} from '../../app/constants/commercium-network';
import { parseMasternodeConf } from './parse-masternode-conf';

try {
  if (fs.existsSync(locateMasternodeConf)) {
    store.set('');
  }
} catch (err) {
  log('No masternode config');
}
const getDaemonOptions = ({
  username, password, useDefaultCommerciumConf, optionsFromCommerciumConf,
}) => {
  /*
    -showmetrics
        Show metrics on stdout
    -metricsui
        Set to 1 for a persistent metrics screen, 0 for sequential metrics
        output
    -metricsrefreshtime
        Number of seconds between metrics refreshes
  */

  const defaultOptions = [
    '-server=1',
    '-showmetrics',
    '--metricsui=0',
    '-metricsrefreshtime=1',
    `-rpcuser=${username}`,
    `-rpcpassword=${password}`,
    ...(isTestnet() ? ['-testnet', '-addnode=testnet.commercium.net'] : ['-addnode=seed01.commercium.net']),
    // Overwriting the settings with values taken from "commercium.conf"
    ...optionsFromCommerciumConf,
  ];

  if (useDefaultCommerciumConf) defaultOptions.push(`-conf=${locateCommerciumConf()}`);

  return Array.from(new Set([...defaultOptions, ...optionsFromCommerciumConf]));
};

let resolved = false;

const COMMERCIUMD_PROCESS_NAME = getDaemonName();

let isWindowOpened = false;

const sendToRenderer = (event: string, message: Object, shouldLog: boolean = true) => {
  if (shouldLog) {
    log(message);
  }

  if (isWindowOpened) {
    if (!mainWindow.isDestroyed()) {
      mainWindow.webContents.send(event, message);
    }
  } else {
    const interval = setInterval(() => {
      if (isWindowOpened) {
        mainWindow.webContents.send(event, message);
        clearInterval(interval);
      }
    }, 1000);
  }
};

// eslint-disable-next-line
const runDaemon: () => Promise<?ChildProcess> = () => new Promise(async (resolve, reject) => {
  mainWindow.webContents.on('dom-ready', () => {
    isWindowOpened = true;
  });

  const processName = path.join(getBinariesPath(), getOsFolder(), COMMERCIUMD_PROCESS_NAME);
  const isRelaunch = Boolean(process.argv.find(arg => arg === '--relaunch'));

  if (!mainWindow.isDestroyed()) mainWindow.webContents.send('commerciumd-params-download', 'Fetching params...');

  sendToRenderer('commercium-daemon-status', {
    error: false,
    status:
        'Downloading network params, this may take some time depending on your connection speed',
  });

  const [err] = await eres(fetchParams());

  if (err) {
    sendToRenderer('commercium-daemon-status', {
      error: true,
      status: `Error while fetching params: ${err.message}`,
    });

    return reject(new Error(err));
  }

  sendToRenderer('commercium-daemon-status', {
    error: false,
    status: 'Millennium Starting',
  });

  // In case of --relaunch on argv, we need wait to close the old commercium daemon
  // a workaround is use a interval to check if there is a old process running
  if (isRelaunch) {
    await waitForDaemonClose(COMMERCIUMD_PROCESS_NAME);
  }

  const [, isRunning] = await eres(processExists(COMMERCIUMD_PROCESS_NAME));

  // This will parse and save rpcuser and rpcpassword in the store
  let [, optionsFromCommerciumConf] = await eres(parseCommerciumConf());
  const [, configFromMasternodeConf] = await eres(parseMasternodeConf());
  store.set('masternode_conf', configFromMasternodeConf);

  // if the user has a custom datadir and doesn't have a commercium.conf in that folder,
  // we need to use the default commercium.conf
  let useDefaultCommerciumConf = false;

  if (optionsFromCommerciumConf.datadir) {
    const hasDatadirConf = fs.existsSync(path.join(optionsFromCommerciumConf.datadir, 'commercium.conf'));

    if (hasDatadirConf) {
      optionsFromCommerciumConf = await parseCommerciumConf(
        path.join(String(optionsFromCommerciumConf.datadir), 'commercium.conf'),
      );
    } else {
      useDefaultCommerciumConf = true;
    }
  }

  if (optionsFromCommerciumConf.rpcuser) store.set('rpcuser', optionsFromCommerciumConf.rpcuser);
  if (optionsFromCommerciumConf.rpcpassword) store.set('rpcpassword', optionsFromCommerciumConf.rpcpassword);
  if (optionsFromCommerciumConf.rpcport) store.set('rpcport', optionsFromCommerciumConf.rpcport);

  if (isRunning) {
    log('Already is running!');

    store.set(EMBEDDED_DAEMON, false);
    // We need grab the rpcuser and rpcpassword from either process args or commercium.conf

    // Command line args override commercium.conf
    const [{ cmd }] = await findProcess('name', COMMERCIUMD_PROCESS_NAME);
    const {
      user, password, port, isTestnet: isTestnetFromCmd,
    } = parseCmdArgs(cmd);

    store.set(
      COMMERCIUM_NETWORK,
      isTestnetFromCmd || optionsFromCommerciumConf.testnet === '1' ? TESTNET : MAINNET,
    );

    if (user) store.set('rpcuser', user);
    if (password) store.set('rpcpassword', password);
    if (!port) {
      store.set('rpcport', 12020);
    } else {
      store.set('rpcport', port);
    }

    return resolve();
  }

  store.set(EMBEDDED_DAEMON, true);

  if (!isRelaunch) {
    store.set(COMMERCIUM_NETWORK, optionsFromCommerciumConf.testnet === '1' ? TESTNET : MAINNET);
  }

  if (!optionsFromCommerciumConf.rpcuser) store.set('rpcuser', uuid());
  if (!optionsFromCommerciumConf.rpcpassword) store.set('rpcpassword', uuid());
  if (!optionsFromCommerciumConf.rpcport) store.set('rpcport', '12020');

  const rpcCredentials = {
    username: store.get('rpcuser'),
    password: store.get('rpcpassword'),
  };

  if (isDev) log('Rpc Credentials', rpcCredentials);

  const childProcess = cp.spawn(
    processName,
    getDaemonOptions({
      ...rpcCredentials,
      useDefaultCommerciumConf,
      optionsFromCommerciumConf: generateArgsFromConf(optionsFromCommerciumConf),
    }),
    {
      stdio: ['ignore', 'pipe', 'pipe'],
    },
  );

  childProcess.stdout.on('data', (data) => {
    sendToRenderer('commerciumd-log', data.toString(), false);
    if (!resolved) {
      resolve(childProcess);
      resolved = true;
    }
  });

  childProcess.stderr.on('data', (data) => {
    log(data.toString());
    reject(new Error(data.toString()));
  });

  childProcess.on('error', reject);

  if (os.platform() === 'win32') {
    resolved = true;
    resolve(childProcess);
  }
});

// eslint-disable-next-line
export default runDaemon;
