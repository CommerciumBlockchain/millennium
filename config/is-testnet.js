// @flow

import electronStore from './electron-store';
import { COMMERCIUM_NETWORK, MAINNET } from '../app/constants/commercium-network';

export const isTestnet = () => electronStore.get(COMMERCIUM_NETWORK) !== MAINNET;
