// @flow

import { isTestnet } from '../../config/is-testnet';

export const COMMERCIUM_EXPLORER_BASE_URL = isTestnet()
  ? 'https://explorer.testnet.commercium.net/tx/'
  : 'https://explorer.commercium.net/tx/';
