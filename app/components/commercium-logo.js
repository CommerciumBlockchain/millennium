// @flow
import React from 'react';

export const CommerciumLogo = () => (
  <svg viewBox='0 0 250 250' width='100%' height='100%'>
    <defs>
      <linearGradient id='a' x1='17.95' y1='24.71' x2='168.95' y2='166.2' gradientTransform='matrix(1, 0, 0, -1, 0, 184)' gradientUnits='userSpaceOnUse'>
        <stop offset='0' stopColor='#0097ff' />
        <stop offset='1' stopColor='#36dbe1' />
      </linearGradient>
    </defs>
    <circle cx='90.8' cy='90.8' r='90.8' style={{ fill: 'none' }} />
    <path transform='translate(0 70)' d='M94,.1A89.87,89.87,0,0,0,36.4,18.2a13.58,13.58,0,0,0-.9,20.9h0a13.61,13.61,0,0,0,17.4.8A63.65,63.65,0,0,1,92.3,26.4a64.4,64.4,0,0,1,47,20.5c4.4,4.7,4.8,12.2.3,16.9a12.07,12.07,0,0,1-17.7-.1,40,40,0,1,0,.1,54,11.77,11.77,0,0,1,14.2-2.6,12.13,12.13,0,0,1,3.6,19A64.59,64.59,0,0,1,92.3,155,63.58,63.58,0,0,1,53,141.5a13.61,13.61,0,0,0-17.4.8h0a13.67,13.67,0,0,0,1.1,21.1A90.53,90.53,0,0,0,181.4,91.5C181.9,42.4,142.9,1.7,94,.1Zm-1.7,113a22.5,22.5,0,1,1,22.5-22.5A22.53,22.53,0,0,1,92.3,113.1Z' style={{ fill: 'url(#a)' }}/>
  </svg>
);
