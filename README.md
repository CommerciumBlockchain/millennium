# Commercium Millennium

Millennium wallet is based on Zepio which is a Sapling-enabled shielded-addres-first wallet featuring cross-platform applications (macOS, Windows and Linux), built-in full node with support for `mainnet` and `testnet`, as well as `dark` and `light` themes.

![Flow Coverage](./public/flow-coverage-badge.svg)

## Stack Information

List of the main open source libraries and technologies used in building **Millenium**:

- [commerciumd](https://github.com/CommerciumBlockchain/Commercium): Commercium node daemon
- [Electron](https://github.com/electron/electron): Desktop application builder
- [React](https://facebook.github.io/react/): User interface view layer
- [Redux](http://redux.js.org/): Predictable application state container
- [Styled Components](https://www.styled-components.com/): Visual primitives for theming and styling applications
- [webpack](http://webpack.github.io/): Application module bundler (and more)
- [Babel](http://babeljs.io/): ES7/JSX transpilling
- [ESLint](http://eslint.org/): Code linting rules
- [Flow](https://flow.org): JavaScript static type checker
- [Docz](https://docz.site): Documentation builder

## Installing and Running From Source

To run from source 
```bash
# Ensure you have Node LTS v8+
# https://nodejs.org/en/

# Clone Codebase

git clone https://CommerciumBlockchain@bitbucket.org/CommerciumBlockchain/millennium.git

# Install Dependencies
# inside of the `millennium` folder
yarn install
# or
npm install

# Start Application
# webpack development server hosts the application on port
# 8080 and launches the Electron wrapper, which also hosts
# the `commerciumd` node daemon process.
yarn start
# or
npm start
```

## Building Application Locally

To build the application locally follow the instructions below:
```bash
# Make sure you are inside of the main `millennium` folder

# Run Build Script
yarn electron:distall

# Executables and binaries available under `/dist` folder
```

## Flow Coverage (Static Type Checker)

For a deeper look on the static typing coverage of the application, please follow below:
```bash
# Make sure you are inside of the main `millenium` folder

# Generate Flow Coverage Report
# this can take a couple seconds
yarn flow:report

# Browser should open with the file `index.html` opened
# Files are also available at `millenium/flow-coverage/source`
```

To run the component library locally, run the following:
```bash
# Make sure you are inside of the main `millenium` folder

# Run Docz Development Script
yarn docz:dev

# Visit http://127.0.0.1:4000/
```

To build the component library locally, run the following:
```bash
# Make sure you are inside of the main `millenium` folder

# Run Build Script
yarn docz:build

# Check `/.docz/dist` folder for built static assets
```

## Tests

To run the application's tests, please run the below:
```bash
# Make sure you are inside of the main `millenium` folder

# For Unit Tests: Run Jest Unit Test Suite
yarn test:unit

# For E2E (end-to-end) Tests: Run Jest E2E Suite
yarn e2e:serve
# on another terminal window
yarn test e2e
```
## License

MIT © Zcash Foundation 2019 zfnd.org
MIT Commercium Blockchain Technologies 2019 commercium.net
