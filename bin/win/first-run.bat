@echo off

IF NOT EXIST %AppData%\Commercium (
    mkdir %AppData%\Commercium
)

IF NOT EXIST %AppData%\ZcashParams (
    mkdir %AppData%\ZcashParams
)

IF NOT EXIST %AppData%\Commercium\commercium.conf (
   (
    echo addnode=seed01.commercium.net
    echo rpcuser=username
    echo rpcpassword=password%random%%random%
    echo daemon=1
    echo showmetrics=0
    echo gen=0
) > %AppData%\Commercium\commercium.conf
)

IF NOT EXIST %AppData%\Commercium\wallet.datm (
    copy %AppData%\Commercium\wallet.dat %AppData%\Commercium\wallet.datm
)

IF NOT EXIST %AppData%\Commercium\masternode.conf (
   (
    echo # Masternode config file
    echo # Format: alias IP:port masternodeprivkey collateral_output_txid collateral_output_index
    echo # Example: mn1 123.123.123.123:2019 5iHaYBVUCYjEMeeH1Y4sBGLALQZE1Yc1K64xiqgX37tGBDQL8Xg 2bcd3c84c84f87eaa86e4e56834c92927a07f9e18718810b92e0d0324456a67c 0
) > %AppData%\Commercium\masternode.conf
)

