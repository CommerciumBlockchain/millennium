// @flow

import getCMMPrice from '../../services/cmm-price';

describe('CMM PRICE Services', () => {
  test('should return the right value', async () => {
    const response = await getCMMPrice(['TRY', 'EUR', 'USD']);

    expect(response).toEqual({
      usd: expect.any(Number),
      try: expect.any(Number),
      eur: expect.any(Number),
    });
  });
});
