// @flow

import got from 'got';

type Payload = {
  [currency: string]: number,
};

/**
  WARNING:
  Just a super fast way to get the zec price
*/
// eslint-disable-next-line
export default (currencies: string[] = ['USD']): Promise<Payload> => new Promise((resolve, reject) => {
  const ENDPOINT = `https://api.coingecko.com/api/v3/simple/price?ids=commercium&vs_currencies=${currencies.join(
    ',',
  )}&api_key=${String(process.env.CMM_PRICE_API_KEY)}`;

  got(ENDPOINT)
    .then(response => resolve(JSON.parse(response.body).commercium))
    .catch(reject);
});
